#ifndef _LOADBALANCER_
#define _LOADBALANCER_
#ifndef _THREAD_
#include <thread>
#endif

#ifndef _VECTOR_
#include <vector>
#endif

#ifndef _FUNCTIONAL_
#include <functional>
#endif

#pragma once

class threadTask
{
public:
	std::thread thr;
	std::vector<std::function<void()>> q;
	std::atomic<bool> running = true;
	threadTask()
	{

	}

	void DetachThread() {
		this->thr.detach();
	}
};

class bossTask : threadTask {
	std::vector<workerTask>* workers;
public:
	void boss()
	{
		while (this->running)
		{
			if (this->q.size() > 0)
			{
				for (auto& w : *workers)
				{
					if (w.GetStatus())
					{
						w.AssignTask(q.front());
						q.erase(q.begin(), q.begin() + 1);
					}
				}
			}
		}
	}

	bossTask()
	{
		this->thr = std::thread([this] { this->boss(); });
	}

	void setWorkerThreads(std::vector<workerTask>& w)
	{
		this->workers = &w;
	}

};

class workerTask : threadTask {

	std::atomic<bool> busy = false;
public:


	void worker() 
	{
		while (this->running)
		{
			if (this->q.size() == 0)
			{
				std::cout << std::this_thread::get_id() << " is idle" << std::endl;
			}
			else
			{
				this->q.front();
			}
		}
	}

	workerTask()
	{
		this->thr = std::thread([this] { this->worker(); });
	}

	bool GetStatus() 
	{
		return this->busy;
	}

	void AssignTask(std::function<void()> task) 
	{
		this->q.push_back(task);
	}
};

class LoadBalancer
{
private:
	std::vector<workerTask> workers;
	bossTask bossThread;

public:
	
	LoadBalancer() 
	{
		for (int i = 1; i < std::thread::hardware_concurrency(); ++i)
		{
			this->workers.push_back(workerTask());
		}
		bossThread.setWorkerThreads(this->workers);
	}
};
#endif