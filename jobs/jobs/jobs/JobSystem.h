#ifndef _JOBSYSTEM_
#define _JOBSYSTEM_
#ifndef _THREAD_
#include <thread>
#endif

#ifndef _VECTOR_
#include <vector>
#endif

#ifndef _FUNCTIONAL_
#include <functional>
#endif

#ifndef _ALGORITHM_
#include <algorithm>
#endif

#ifndef GUID_DEFINED
#include <guiddef.h>
#include <combaseapi.h>
#endif

#ifndef _CONDITION_VARIABLE_
#include <condition_variable>
#endif

#define READY -1
#define LOCKED -2

#pragma once
std::condition_variable threadsLocked;	//cv to idle threads
std::mutex threadsLocked_mutex;			//mutex for above


class threadFunction
{
private:
	std::function<void()> task;			//tasks stored in threadFunction
	std::vector<GUID> dependencies;		//dependencies of task above
	GUID guid;							//this threadFunction's GUID

public:

	bool operator==(threadFunction& other)	//compare to threadfunctions
	{
		return this->guid == other.guid;
	}

	//checks dependencies and returns true if all are fulfilled
	bool GetDependencies(std::vector<threadFunction*>* tasksDone, GUID runningTask = GUID_NULL)
	{
		size_t counter = this->dependencies.size();		//get amount of dependencies
		
		if (counter != 0)	//if there are dependencies there is more work to do
		{
			for (GUID g : this->dependencies)	//iterate through all dependencies
			{
				for (auto it = tasksDone->begin(); it != tasksDone->end(); ++it)	//iterate through all tasks done
				{
					auto& tf = *it;
				
					if (tf == nullptr) break;	//if tf is nullptr -> this task hasn't been done yet
					if (tf->GetGuid() == g || runningTask == g)	//compare tasks GUID to current dependency
					{
						--counter;	//if match -> one has been fulfilled
					}
				}
			}
		}
		return !counter;	//if counter == 0 then !counter = true; if counter > 0 then !counter = false; if counter < 0 then something somewhere went terribly wrong
	}

	//constructor
	threadFunction(std::function<void()> t, std::vector<GUID> dep = {})
	{
		this->task = t;				//store task
		this->dependencies = dep;	//store dependencies

		HRESULT guid_res = CoCreateGuid(&this->guid);	//generate GUID
	}

	//copy constructor
	threadFunction(const threadFunction& tf)
	{
		this->task = tf.task;
		this->dependencies = tf.dependencies;
		this->guid = tf.guid;
	}

	//destructor
	~threadFunction()
	{
		this->dependencies.clear();		//clear vector
		this->task = nullptr;			//clear task
	}

	//returns task
	std::function<void()>& GetTask()	
	{
		return this->task;
	}

	//returns guid
	GUID GetGuid() 
	{
		return this->guid;
	}
};

std::mutex tasks_m;	//used to lock tasks
std::vector<threadFunction*> tasksToDo;	//list of tasks to do
std::vector<std::pair<threadFunction*, int>> tasksStandBy;	//list of tasks ready and waiting for execution
std::mutex tasksStandBy_m;	//used to lock tasksStandBy
std::vector<threadFunction*> tasksRunning;	//list of running tasks (actively and waiting for execution)
std::vector<threadFunction*> tasksDone;		//list of all completed tasks

class workerTask
{
private:
	std::thread thr;	//threaded function
	//task and id in vector
		std::function<void()> task;
		int mem;
	//####
	std::atomic<bool> running = true;	//keep the loop going
	int state = READY;	//current state of worker

	//this method is done mainly by the thread
	void loop()
	{
		while (this->running)	//always true
		{
			if (this->state == READY)	//only ready workers are alowed to get a new task
			{
				tasksStandBy_m.lock();	//lock vector to not fuck things up
				if (tasksStandBy.size() > 0)	//check if there is work on standby to do
				{
					//FIFO
					this->task = tasksStandBy.front().first->GetTask();		//the oldest task added to the queue
					this->mem = tasksStandBy.front().second;				//the index of the tasks position in the ToDo/Running/Done vectors is stored here
					tasksStandBy.erase(tasksStandBy.begin());				//remove the task from Standby as it is about to be processed
					tasksStandBy_m.unlock();								//vector manipulation done -> unlock

					this->state = LOCKED;									//thread is no longer ready -> lock it
					this->task();											//execute task
					this->task = nullptr;									//task done -> clear variable
					this->state = this->mem;								//state is set to vector index
					this->mem = READY;										//mem is no longer needed; this is just to be safe
				}
				else
				{
					tasksStandBy_m.unlock();								//we need to unlock the vector if there is nothing to do as well
				}
			} 
			{   //here no else, only a block for the scope of the mutex to keep it locked as short as possible
				std::unique_lock<std::mutex> threadLock(threadsLocked_mutex);	//pause the thread until notification to keep CPU load from going over 9000 *angry_Vegeta.gif*
				threadsLocked.wait(threadLock, [] {								//unlocks on notify if vector is not empty
					//tasksStandBy_m.lock();									//vec->size() by itself should be thread safe; on possible change it will first be locked and checked again
					bool ret = tasksStandBy.size() > 0;							//check the size of the standby vector on notify				
					//tasksStandBy_m.unlock();									//would be needed if vector was locked (i know we lock the mutex not the vector but it makes more sense to see the vector as locked)
					return ret;													//we return ret ;P
				}); 
			}
		}
	}
public:

	//constructor
	workerTask(int core)
	{
		this->thr = std::thread([this] { this->loop(); });	//initiate the threaded function
		SetThreadAffinityMask(this->thr.native_handle(), DWORD_PTR(1) << core);	//core affinity to assign the thread to a hardware thread
	}

	//destructor
	~workerTask()
	{
		this->running = false;
		this->thr.join();	//join all threads again
	}

	void Reset()	//reset the worker to be ready for new tasks
	{
		this->mem = READY;
		this->state = READY;
	}

	int GetState()	//return current state of worker
	{
		return this->state;
	}
};

class JobSystem
{
private:
	std::vector<workerTask*> worker;	//vector of workers
	std::vector<threadFunction> tasks;	//vector of tasks
	int task_Counter = 0;				//counter variable
	size_t task_Amount = 0;				//compare variable

public:

	//constructor; create a worker per hardware thread (-1 to keep resources for main thread)
	JobSystem()
	{
		//start thread affinity at core 1 so core 0 is for main task
		for (unsigned int i = 1; i < std::thread::hardware_concurrency(); ++i)
		{
			worker.push_back(new workerTask(i));
		}
	}


	//Registers a new Task to the jobsystem, can be given dependencies to other tasks in form of std::vector<GUID>
	GUID RegisterNewTask(std::function<void()> task, std::vector<GUID> dep_guids = {})
	{
		tasks_m.lock();		//tasks should not be added during a tick

		tasks.push_back(std::move(threadFunction(task, dep_guids)));

		//later pushbacks would fuck up the pointer so we basically reset the lists every time a task is added
		tasksToDo.clear();
		tasksRunning.clear();
		tasksDone.clear();

		//all three lists need to be of equal size for the jobsystem to work
		for (int i = 0; i < tasks.size(); ++i)
		{
			tasksToDo.push_back(&tasks[i]);
			tasksRunning.push_back(nullptr);
			tasksDone.push_back(nullptr);
		}

		tasks_m.unlock();
		return tasks.back().GetGuid();	//return GUID for dependencies
	}


	void Tick()
	{
		this->task_Counter = 0;	//how many threads are done -> 0 at start of tick

		tasks_m.lock();	//task list should not change during a tick, so we lock it here

		this->task_Amount = tasksToDo.size();	//how many tasks to do
		do
		{
			{	//block for mutex scope -> makes stuff a little faster :)
				std::lock_guard<std::mutex> lk(threadsLocked_mutex);
				for (int i = 0; i < tasksToDo.size(); ++i)	//iterate through all tasks
				{
					if (tasksToDo[i] != nullptr && tasksToDo[i]->GetDependencies(&tasksDone))	//is there still a task at this index of tasksToDo (or is there a nullptr = Task is already done; also check dependencies)
					{
						tasksStandBy_m.lock();	//lock vector to not fuck things up
						tasksStandBy.push_back(std::pair<threadFunction*, int>(tasksToDo[i], i));	//add the task to the standby/ready (whatever you want to call it) list
						tasksStandBy_m.unlock(); //vector manipulation is done->unlock
						std::swap(tasksToDo[i], tasksRunning[i]);	//move the task to running as it has been added to the queue
						threadsLocked.notify_one();	//notify a thread to pick up the task -> if no thread is idle system would lock up once all tasks are out of tasksToDo -> line 263 is needed
					}
				}
				threadsLocked.notify_one();	//if there are less threads than available tasks system would lock up 
			}
			for (workerTask* w : worker)	//iterate through all worker threads
			{
				int j = w->GetState();	//get thread state
				if (j >= 0)	//if state is not READY or LOCKED it must be finished
				{
					std::swap(tasksRunning[j], tasksDone[j]);	//get the task from running to tasksDone <- this vector is checked for dependencies
					++task_Counter;	//we finished a task; get ourselfes a cookie and be proud 
					w->Reset();	//tell the worker to be ready for further action
				}
			}
		} while (this->task_Counter != this->task_Amount);	//done when all tasks are finished
		tasksToDo.swap(tasksDone);	//all tasks need to be undone again for next tick
		tasks_m.unlock();	//vector manipulation done, now new tasks may be added
	}
};
#endif