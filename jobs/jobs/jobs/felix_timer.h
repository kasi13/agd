#pragma once

#ifndef FELIX_TIMING
#define FELIX_TIMING

#ifndef _CHRONO_
#include <chrono>
#endif

#ifndef _VECTOR_
#include <vector>
#endif

#include <fstream>

std::vector<std::pair<std::chrono::steady_clock::time_point, std::chrono::steady_clock::time_point>> timings;


//auto start = chrono::high_resolution_clock::now();

void ExportFelixLog() 
{
#ifndef NO_REMOTERY
	std::ofstream output(".\\logs\\logfile_WITH_REMOTERY.txt");
#else
	std::ofstream output(".\\logs\\logfile_NO_REMOTERY.txt");
#endif
	
	std::cout << "printing log" << std::endl;
	if (output.is_open())
	{
		int i = 0;
		for (std::pair<std::chrono::steady_clock::time_point, std::chrono::steady_clock::time_point> t : timings)
		{
			output << ++i << ": ";
			output << std::chrono::duration_cast<std::chrono::microseconds>(t.second - t.first).count()<< "us\n";
		}
	}
	output.close();
	std::cout << "done" << std::endl;
}


#endif