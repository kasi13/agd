#include "pch.h"

/*
 * Felix Deschmann <gs19m001>, Felix Ka�berger <gs19m007>
 */


/* custom timing and debug stuff

*	#include "felix_timer.h"
*	#include "testMethods.h"
*	#define DEBUG
*	#define NO_REMOTERY

*/


// Remotery is a easy to use profiler that can help you with seeing execution order and measuring data for your implementation
// Once initialized, you can use it by going into the "vis" folder of the project and opening "vis/index.html" in a browser (double-click)
// It emits 3 warnings at the moment, does can be ignored and will not count as warnings emitted by your code! ;)
// 
// Github: https://github.com/Celtoys/Remotery
#include "Remotery/Remotery.h"
#include "JobSystem.h"

using namespace std;

#define MAKE_UPDATE_FUNC(NAME, DURATION, STRING) \
	void Update##NAME() { \
		rmt_ScopedCPUSample(NAME, 0); \
		auto start = chrono::high_resolution_clock::now(); \
		decltype(start) end; \
		do { \
			end = chrono::high_resolution_clock::now(); \
		} while (chrono::duration_cast<chrono::microseconds>(end - start).count() < (DURATION)); \
	} \

// You can create other functions for testing purposes but those here need to run in your job system
// The dependencies are noted on the right side of the functions, the implementation should be able to set them up so they are not violated and run in that order!
MAKE_UPDATE_FUNC(Input, 200, "1 Input\n") // no dependencies
MAKE_UPDATE_FUNC(Physics, 1000, "2 Physics\n") // depends on Input
MAKE_UPDATE_FUNC(Collision, 1200, "3 Collision\n") // depends on Physics
MAKE_UPDATE_FUNC(Animation, 600, "4 Animation\n") // depends on Collision
MAKE_UPDATE_FUNC(Particles, 800, "5 Particles\n") // depends on Collision
MAKE_UPDATE_FUNC(GameElements, 2400, "6 GameElements\n") // depends on Physics
MAKE_UPDATE_FUNC(Rendering, 2000, "7 Rendering\n") // depends on Animation, Particles, GameElements
MAKE_UPDATE_FUNC(Sound, 1000, "8 Sound\n") // no dependencies

void UpdateSerial()	//unused but kept in
{
#ifndef NO_REMOTERY
	rmt_ScopedCPUSample(UpdateSerial, 0);
#endif
	UpdateInput();
	UpdatePhysics();
	UpdateCollision();
	UpdateAnimation();
	UpdateParticles();
	UpdateGameElements();
	UpdateRendering();
	UpdateSound();
}

static JobSystem js;

// In `UpdateParallel` you should use your jobsystem to distribute the tasks
// Tasks need to be already registered here otherwise JobSystem.Tick() does the rest
void UpdateParallel()
{
#ifndef NO_REMOTERY
	rmt_ScopedCPUSample(UpdateParallel, 0);
#endif
	js.Tick();
}

#ifdef DEBUG
void RegisterParallel()
{
	GUID DEP_INP = js.RegisterNewTask(Input);
	GUID DEP_PHY = js.RegisterNewTask(Physics, { DEP_INP });
	GUID DEP_COL = js.RegisterNewTask(Collision, { DEP_PHY });
	GUID DEP_ANI = js.RegisterNewTask(Animation, { DEP_COL });
	GUID DEP_PAR = js.RegisterNewTask(Particles, { DEP_COL });
	GUID DEP_GAM = js.RegisterNewTask(GameElements, { DEP_PHY });
	GUID DEP_REN = js.RegisterNewTask(Rendering, { DEP_ANI, DEP_PAR, DEP_GAM });
	GUID DEP_SOU = js.RegisterNewTask(Sound);
	GUID DEP_RES = js.RegisterNewTask(Reset, { DEP_INP, DEP_PHY, DEP_COL, DEP_ANI, DEP_PAR, DEP_GAM, DEP_REN, DEP_SOU});
}
#else

//Register Tasks once and use their returned GUIDs for dependencies of following tasks
void RegisterParallel()
{
	GUID DEP_INP = js.RegisterNewTask(UpdateInput);
	GUID DEP_PHY = js.RegisterNewTask(UpdatePhysics, { DEP_INP });
	GUID DEP_COL = js.RegisterNewTask(UpdateCollision, { DEP_PHY });
	GUID DEP_ANI = js.RegisterNewTask(UpdateAnimation, { DEP_COL });
	GUID DEP_PAR = js.RegisterNewTask(UpdateParticles, { DEP_COL });
	GUID DEP_GAM = js.RegisterNewTask(UpdateGameElements, { DEP_PHY });
	GUID DEP_REN = js.RegisterNewTask(UpdateRendering, { DEP_ANI, DEP_PAR, DEP_GAM });
	GUID DEP_SOU = js.RegisterNewTask(UpdateSound);
}
#endif

void UpdateParallel_Untouched() //for reference; unused
{
#ifndef NO_REMOTERY
	rmt_ScopedCPUSample(UpdateParallel_Untouched, 0);
#endif
	UpdateInput();
	UpdatePhysics();
	UpdateCollision();
	UpdateAnimation();
	UpdateParticles();
	UpdateGameElements();
	UpdateRendering();
	UpdateSound();
}

int main()
{
	/*
	 * This initializes remotery, you are not forced to use it (although it's helpful)
	 * but please also don't remove it from here then. Because if you don't use it, I
	 * will most likely do so, to track how your jobs are issued and if the dependencies run
	 * properly
	 */

#ifndef NO_REMOTERY
	Remotery* rmt;
	rmt_CreateGlobalInstance(&rmt);
#endif

	atomic<bool> isRunning = true;

	//unused and unchanged serial function from assignment
	/*thread serial([&isRunning]()
	{
		while (isRunning)
			UpdateSerial();
	});*/

	thread parallel([&isRunning]()
	{
		//Register Tasks before trying to use them!
		RegisterParallel();
		while (isRunning)
		{
			auto start = chrono::high_resolution_clock::now();	//custom timing
			UpdateParallel();	//could already be JobSystem.Tick() but tried to keep to the original structure of the assignment
			#ifdef FELIX_TIMING
			timings.push_back(std::pair<std::chrono::steady_clock::time_point, std::chrono::steady_clock::time_point>(start, chrono::high_resolution_clock::now())); //custom timing
			std::cout << std::chrono::duration_cast<std::chrono::microseconds>(timings.back().second - timings.back().first).count() << "us" << std::endl; //custom timing
			#endif
		}
	});
	SetThreadAffinityMask(parallel.native_handle(), DWORD_PTR(1) << 0); // onto thread 0 -> others initiated will start at thread 6 to 11 (on my i7-5820K cpu)

	//unused and unchanged parallel function from assignment
	/*thread parallel_ref([&isRunning]()
		{
			while (isRunning)
				UpdateParallel_Untouched();
		});*/

	cout << "Type anything to quit...\n";
	char c;
	cin >> c;
	cout << "Quitting...\n";
	isRunning = false;

	//serial.join();
	parallel.join();
	//parallel_ref.join();
#ifndef NO_REMOTERY
	rmt_DestroyGlobalInstance(rmt);
#endif
#ifdef FELIX_TIMING	//custom timing
	ExportFelixLog();
#endif
}

