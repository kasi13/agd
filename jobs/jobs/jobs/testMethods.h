#include "pch.h"

#pragma once

bool inp = false;
bool phy = false;
bool col = false;
bool ani = false;
bool par = false;
bool gam = false;
bool ren = false;
bool sou = false;

void Input() 
{
	inp = true;
}

void Physics() 
{
	if (!inp) throw "Dependencies not fullfilled";
	phy = true;
}

void Collision() 
{
	if (!phy) throw "Dependencies not fullfilled";
	col = true;
}

void Animation() 
{
	if (!col) throw "Dependencies not fullfilled";
	ani = true;
}

void Particles() 
{
	if (!col) throw "Dependencies not fullfilled";
	par = true;
}

void GameElements() 
{
	if (!phy) throw "Dependencies not fullfilled";
	gam = true;
}

void Rendering() 
{
	if (!ani || !par || !gam) throw "Dependencies not fullfilled";
	ren = true;
}

void Sound() 
{
	sou = true;
}

void Reset() 
{
	if (!inp || !phy || !col || !ani || !par || !gam || !ren || !sou) throw "Dependencies not fullfilled";
	inp = false;
	phy = false;
	col = false;
	ani = false;
	par = false;
	gam = false;
	ren = false;
	sou = false;
	std::cout << ".";
}